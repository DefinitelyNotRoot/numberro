FROM python:3.8 as base

RUN mkdir /app
WORKDIR "/app"
ADD main.py /app
ADD test.py /app
ENTRYPOINT  [ "python3", "test.py" ]


FROM base as release

COPY --from=base  / /
ENTRYPOINT  [ "python3", "main.py" ]