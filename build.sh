#!/usr/bin/env bash
set -e 
echo "Build base:"
docker build --target=base -t numberro . 
echo "Run tests:"
docker run -it numberro
echo "Build release:"
docker build  --target=release -t numberro .
echo -e "You can now run this cli from docker with: 'docker run -it numberro num1 num2 num3 num4 num5 num6' cmd.\nExample: docker run -it numberro 1 2 3 4 5 6"
