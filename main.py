import sys
import argparse
from functools import reduce
import random


def pickRandom(numbers):
    return random.choice(numbers)


def substractionNumbers(numbers):
    numberlist=numbers.copy() # Copy because i dont want to modify actual number list
    subs=reduce(lambda a, b: a-b, numberlist) 
    numberlist.append(subs)
    return ",".join(str(number) for number in numberlist)


def sortNumbers(numbers, reverse=False):
    return sorted(numbers, reverse=reverse)


def multiplicationNumbers(numbers):
    directory = {}
    for number, counter in zip(numbers, range(1, 7)):
        directory['InputNumber{}'.format(counter)] = number
    directory['Multiplication'] = reduce(lambda a, b: a*b, numbers)
    return directory

def validationNumbers(numbers, max_numbers=6):
    if (len(numbers) > max_numbers):
        print("Please provide only {} numers!".format(max_numbers))
        return False
    if sortNumbers(numbers) == list(range(min(numbers), max(numbers)+1)):
        return True
    else:
        print("Not consecutive numbers")
        return False




def menu(numbers):
    while True:
        print("""
        1. Perform subtraction and show output on screen comma separated.
        2. Perform multiplication and store result in a JSON file (i.e. {“InputNumber1”: x, “InputNumber2”: y, “InputNumber3”: a, “InputNumber4”: b,
         “InputNumber5”: c, “InputNumber6”: d, “Multiplication”: X }, where x, y, a, b, c and d are user line arguments and X is the multiplication result)
        3. Pick randomly a number and show it on screen.
        4. Print sorted (highest to lowest) array/list numbers.
        5. Print sorted (lowest to highest) array/list numbers.
        """)
        try:
            userInput = int(input("Enter a choice:"))
            if userInput == 1:
                print("output: {}".format(substractionNumbers(numbers)))
            elif userInput == 2:
                print("output: {}".format(multiplicationNumbers(numbers)))
            elif userInput == 3:
                print("output: {}".format(pickRandom(numbers)))
            elif userInput == 4:
                print("output: {}".format(sortNumbers(numbers)))
            elif userInput == 5:
                print("output: {}".format(sortNumbers(numbers, True)))
            else:
                print("Invalid input, try again!")
        except ValueError:
            print("Invalid input, try again!")


def main():
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('first', metavar='first', type=int,
                        help='first argument')
    parser.add_argument('second', metavar='second', type=int,
                        help='first argument')
    parser.add_argument('third', metavar='thirdr', type=int,
                        help='an integer for the accumulator')
    parser.add_argument('fourth', metavar='fourthr', type=int,
                        help='an integer for the accumulator')
    parser.add_argument('fifth', metavar='fifth', type=int,
                        help='an integer for the accumulator')
    parser.add_argument('sixth', metavar='sixth', type=int,
                        help='an integer for the accumulator')
    args = parser.parse_args()
    numbers = [args.first,args.second,args.third, args.fourth, args.fifth, args.sixth]
    if validationNumbers(numbers):
        menu(numbers)


if __name__ == "__main__":
    main()
