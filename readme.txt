Numberro 

With this simple cli program you can:
1. Perform subtraction and show output on screen comma separated.
2. Perform multiplication and store result in a JSON file (i.e. {“InputNumber1”: x, “InputNumber2”: y, “InputNumber3”: a, “InputNumber4”: b,
“InputNumber5”: c, “InputNumber6”: d, “Multiplication”: X }, where x, y, a, b, c and d are user line arguments and X is the multiplication result)
3. Pick randomly a number and show it on screen.
4. Print sorted (highest to lowest) array/list numbers.
5. Print sorted (lowest to highest) array/list numbers.


Installation:

* if you have a Python3.8: 
    1. Clone repo
    2. You can run command using python like: `python3 main.py num1 num2 num3 num4 num5 num6` 

* Docker (to avoid mess in your operating system):
    1. Clone repo
    3. Build it with `./build.sh` it will execute:
        * build base image with test entrypoint
        * tests
        * build a "release" image with correct entrypoint

