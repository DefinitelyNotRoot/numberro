import unittest
from main import *


class testNumberCli(unittest.TestCase):
    numbers = [1,2,3,4,5,6]


    def test_pickRandom(self):
        self.assertIn(pickRandom(self.numbers), self.numbers)

    def test_substraction(self):
        self.assertEqual(substractionNumbers(self.numbers), "1,2,3,4,5,6,-19")
        
    def test_sortNumbers(self):
        self.assertEqual(sortNumbers(self.numbers), self.numbers)

            
    def test_sortNumbers_reversed(self):
        self.assertEqual(sortNumbers(self.numbers,True), [6,5,4,3,2,1])


    def test_multiplication(self):
        self.assertEqual(multiplicationNumbers(self.numbers), {'InputNumber1': 1, 'InputNumber2': 2, 'InputNumber3': 3, 'InputNumber4': 4, 'InputNumber5': 5, 'InputNumber6': 6, 'Multiplication': 720})

    def test_numbersValidation_six_numbers_check(self):
        self.assertEqual(validationNumbers(self.numbers), True)

    def test_numbersValidation_consecutive_check(self):
        self.assertEqual(validationNumbers(self.numbers), True)


if __name__ == '__main__':
    unittest.main()